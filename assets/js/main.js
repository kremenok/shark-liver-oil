/*!
* equalize.js
* Author & copyright (c) 2012: Tim Svensen
* Dual MIT & GPL license
*
* Page: http://tsvensen.github.com/equalize.js
* Repo: https://github.com/tsvensen/equalize.js/
*/
!function(i){i.fn.equalize=function(e){var n,t,h=!1,c=!1;return i.isPlainObject(e)?(n=e.equalize||"height",h=e.children||!1,c=e.reset||!1):n=e||"height",i.isFunction(i.fn[n])?(t=0<n.indexOf("eight")?"height":"width",this.each(function(){var e=h?i(this).find(h):i(this).children(),s=0;e.each(function(){var e=i(this);c&&e.css(t,""),e=e[n](),e>s&&(s=e)}),e.css(t,s+"px")})):!1}}(jQuery);




jQuery(document).ready(function($){

 function aligner() {

	 $('.contacts').equalize( {equalize: 'outerHeight', reset: true, children: 'h3' } );
 }

 aligner();

 $( window ).resize(function() {
	 aligner();
 });


});

var swiper = new Swiper('.swiper-container-articles-mob', {
   speed: 800,
   slidesPerView: 1,
   spaceBetween: 17,
   centeredSlides: true,
   width: 267,
   direction: 'horizontal',
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      autoplay: {
         delay: 5000,
       }
    });
    var swiper = new Swiper('.swiper-container-articles', {
       speed: 800,
       slidesPerView: 3,
       spaceBetween: 17,
       direction: 'horizontal',
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          breakpoints: {
              // when window width is >= 320px
              320: {
                slidesPerView: 1,
                spaceBetween: 17,
                centeredSlides: true,
                width: 267
              },
              // when window width is >= 480px
              480: {
                slidesPerView: 1,
                spaceBetween: 17,
                centeredSlides: true,
                width: 267
              },
              // when window width is >= 640px
              640: {
                slidesPerView: 2,
                spaceBetween: 20
              },
              981: {
                slidesPerView: 3,
                spaceBetween: 20
              }
            },
          /*autoplay: {
             delay: 5000,
           }*/
        });
    //read myBar
    if ($("#myBar").length > 0){
      window.onscroll = function() {myFunction()};

      function myFunction() {
        var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
        var height = document.documentElement.scrollHeight - document.documentElement.clientHeight - 1937;
        var scrolled = (winScroll / height) * 100;
        document.getElementById("myBar").style.width = scrolled + "%";
      }
    }

$('.card__tabs-button').click(function () {
    var openEl = $('.' + $(this).data('open'));

    $(openEl).siblings().removeClass('is-open');
    $(this).siblings().removeClass('is-open');
    $(this).addClass('is-open');
    $(openEl).addClass('is-open');
});

$('.faq__tab').click(function () {
    var openEl = $('.' + $(this).data('open'));

    $(openEl).siblings().removeClass('is-open');
    $(this).siblings().removeClass('is-open');
    $(this).addClass('is-open');
    $(openEl).addClass('is-open');

});

(function () {
    var initAccordion = function (itemsClass, headerClass, bodyClass) {
        var $items = $(itemsClass);

        $items.find(headerClass).on('click', function () {
            $item = $(this).parent();
            $items.not($item).removeClass('is-open');
            $items.not($item).find(headerClass).removeClass('is-open');
            $items.not($item).find(bodyClass).slideUp(250);


            if ($(this).hasClass('is-open')) {
                $item.removeClass('is-open');
                $(this).removeClass('is-open');
                $(this).siblings(bodyClass).slideUp(250);

            } else {
                $item.addClass('is-open');
                $(this).addClass('is-open');
                $(this).siblings(bodyClass).slideDown(250);

            }
        });
    };

    // suppliers
    initAccordion('.faq__inner', '.faq__inner-header', '.faq__inner-body');


})($);

//number
$('.products.related input[type="number"]').niceNumber({

// auto resize the number input
autoSize: true,

// the number of extra character
autoSizeBuffer: 1,

// custom button text
buttonDecrement: '-',
buttonIncrement: "+",

// 'around', 'left', or 'right'
buttonPosition: 'around',

onIncrement: function ($currentInput, amount, settings) {
$currentInput.change();
},
onDecrement: function ($currentInput, amount, settings) {
$currentInput.change();
},

});

setTimeout(function() {
  $('.products input[type="number"]').niceNumber({

  // auto resize the number input
  autoSize: true,

  // the number of extra character
  autoSizeBuffer: 1,

  // custom button text
  buttonDecrement: '-',
  buttonIncrement: "+",

  // 'around', 'left', or 'right'
  buttonPosition: 'around',

  onIncrement: function ($currentInput, amount, settings) {
  $currentInput.change();
  },
  onDecrement: function ($currentInput, amount, settings) {
  $currentInput.change();
  },

  });
    }, 6000);

//menu
if ($("header.fixed-top").length > 0){
(function() {

	let hamburger = {
		nav: document.querySelector('#nav'),
		navToggle: document.querySelector('.nav-toggle'),

		initialize() {
			this.navToggle.addEventListener('click',
        () => { this.toggle(); });
		},

		toggle() {
			this.navToggle.classList.toggle('expanded');
			this.nav.classList.toggle('expanded');
		},
	};

	hamburger.initialize();


}());
}
//header
$(window).scroll(function(){
    if ($(this).scrollTop() > 60) {
       $('header.fixed-top').addClass('bg-header');
    } else {
       $('header.fixed-top').removeClass('bg-header');
    }
});

//animation
AOS.init({
  duration: 1200,
})
AOS.init({disable: 'mobile'});
//slider

var swiper = new Swiper('.swiper-container', {
	 slidesPerView: 1,
	 speed: 800,
      direction: 'vertical',
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      autoplay: {
         delay: 5000,
       }
    });

		var swiper = new Swiper('.swiper-container-product', {
			 slidesPerView: 1,
			 autoHeight: true,
			 speed: 800,
			 direction: 'horizontal',
				navigation: {
		        nextEl: '.swiper-button-next',
		        prevEl: '.swiper-button-prev',
      		},
		    });
				var swiper = new Swiper('.swiper-container-clients', {
					 slidesPerView: 4,
					 spaceBetween: 15,
					 speed: 800,
						navigation: {
								nextEl: '.swiper-button-next',
								prevEl: '.swiper-button-prev',
							},
							breakpoints: {
									// when window width is >= 320px
									320: {
										slidesPerView: 1
									},
									// when window width is >= 480px
									480: {
										slidesPerView: 1
									},
									// when window width is >= 640px
									640: {
										slidesPerView: 2,
										spaceBetween: 20
									},
									981: {
										slidesPerView: 4,
										spaceBetween: 20
									}
								}
						});

						var swiper = new Swiper('.swiper-container-product-ver2', {
							 slidesPerView: 1,
							 autoHeight: true,
							 speed: 800,
							 direction: 'horizontal',
							 pagination: {
										el: '.swiper-pagination',
										clickable: true,
									},
						    });

								var swiper = new Swiper('.swiper-container-certificates', {
									 slidesPerView: 3,
									 spaceBetween: 30,
									 cssMode: true,
									 speed: 800,
									 direction: 'horizontal',
									 mousewheel: {
									 		invert: false,
									 	},
										navigation: {
												nextEl: '.swiper-button-next',
												prevEl: '.swiper-button-prev',
											},
											breakpoints: {
													// when window width is >= 320px
													320: {
														slidesPerView: 1
													},
													// when window width is >= 480px
													480: {
														slidesPerView: 1
													},
													// when window width is >= 640px
													640: {
														slidesPerView: 2,
														spaceBetween: 30
													},
													981: {
														slidesPerView: 3,
														spaceBetween: 30
													}
												}
										});


//pop-up

jQuery('.swiper-container-product .swiper-wrapper').each(function() { // the containers for all your galleries
  jQuery(this).magnificPopup({
    delegate: 'a', // the selector for gallery item
    type: 'image',
    gallery: {
      enabled:true
    }
  });
});

jQuery('.swiper-container-product-ver2 .swiper-wrapper').each(function() { // the containers for all your galleries
  jQuery(this).magnificPopup({
    delegate: 'a', // the selector for gallery item
    type: 'image',
    gallery: {
      enabled:true
    }
  });
});

$('.swiper-container-certificates .swiper-wrapper').magnificPopup({
	delegate: 'a',
	 type: 'image',
	 tLoading: 'Загрузка изображения #%curr%...',
	 gallery: {
	     enabled: true,
	     navigateByImgClick: true,
	     preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
}
})
//facebook comments

//scrollTop
// BY KAREN GRIGORYAN

$(document).ready(function() {
  /******************************
      BOTTOM SCROLL TOP BUTTON
   ******************************/

  // declare variable
  var scrollTop = $(".scrollTop");

  $(window).scroll(function() {
    // declare variable
    var topPos = $(this).scrollTop();

    // if user scrolls down - show scroll to top button
    if (topPos > 100) {
      $(scrollTop).css("opacity", "1");

    } else {
      $(scrollTop).css("opacity", "0");  
    }

  }); // scroll END

  //Click event to scroll to top
  $(scrollTop).click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 1000);
    return false;

  });


}); // ready() END

//remove p:empty
$(".accordion-body p").each(function() {
   var $el = $(this);
   if($.trim($el.html()) == "&nbsp;") {
     $el.remove();
   }
 });

 //counter
 if ($(".counter").length > 0){
 function getTimeRemaining(endtime) {
   var t = Date.parse(endtime) - Date.parse(new Date());
   var seconds = Math.floor((t / 1000) % 60);
   var minutes = Math.floor((t / 1000 / 60) % 60);
   var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
   var days = Math.floor(t / (1000 * 60 * 60 * 24));
   return {
     'total': t,
     'days': days,
     'hours': hours,
     'minutes': minutes,
     'seconds': seconds
   };
 }

 function initializeClock(id, endtime) {
   var clock = document.getElementById(id);
   var daysSpan = clock.querySelector('.days');
   var hoursSpan = clock.querySelector('.hours');
   var minutesSpan = clock.querySelector('.minutes');
   var secondsSpan = clock.querySelector('.seconds');

   function updateClock() {
     var t = getTimeRemaining(endtime);

     daysSpan.innerHTML = t.days;
     hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
     minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
     secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

     if (t.total <= 0) {
       clearInterval(timeinterval);
     }
   }

   updateClock();
   var timeinterval = setInterval(updateClock, 1000);
 }


 var deadline="April 21 2021 00:00:00 GMT+0300"; //for Ukraine
 var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000); // for endless timer
 initializeClock('countdown', deadline);
}
//hide comment Safari

var BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
        this.version = this.searchVersion(navigator.userAgent)
            || this.searchVersion(navigator.appVersion)
            || "an unknown version";
        this.OS = this.searchString(this.dataOS) || "an unknown OS";
    },
    searchString: function (data) {
        for (var i=0;i<data.length;i++)    {
            var dataString = data[i].string;
            var dataProp = data[i].prop;
            this.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString) {
                if (dataString.indexOf(data[i].subString) != -1)
                    return data[i].identity;
            }
            else if (dataProp)
                return data[i].identity;
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
    },
    dataBrowser: [
        {
            string: navigator.userAgent,
            subString: "Chrome",
            identity: "Chrome"
        },
        {     string: navigator.userAgent,
            subString: "OmniWeb",
            versionSearch: "OmniWeb/",
            identity: "OmniWeb"
        },
        {
            string: navigator.vendor,
            subString: "Apple",
            identity: "Safari",
            versionSearch: "Version"
        },
        {
            prop: window.opera,
            identity: "Opera",
            versionSearch: "Version"
        },
        {
            string: navigator.vendor,
            subString: "iCab",
            identity: "iCab"
        },
        {
            string: navigator.vendor,
            subString: "KDE",
            identity: "Konqueror"
        },
        {
            string: navigator.userAgent,
            subString: "Firefox",
            identity: "Firefox"
        },
        {
            string: navigator.vendor,
            subString: "Camino",
            identity: "Camino"
        },
        {        // for newer Netscapes (6+)
            string: navigator.userAgent,
            subString: "Netscape",
            identity: "Netscape"
        },
        {
            string: navigator.userAgent,
            subString: "MSIE",
            identity: "Explorer",
            versionSearch: "MSIE"
        },
        {
            string: navigator.userAgent,
            subString: "Gecko",
            identity: "Mozilla",
            versionSearch: "rv"
        },
        {         // for older Netscapes (4-)
            string: navigator.userAgent,
            subString: "Mozilla",
            identity: "Netscape",
            versionSearch: "Mozilla"
        }
    ],
    dataOS : [
        {
            string: navigator.platform,
            subString: "Win",
            identity: "Windows"
        },
        {
            string: navigator.platform,
            subString: "Mac",
            identity: "Mac"
        },
        {
               string: navigator.userAgent,
               subString: "iPhone",
               identity: "iPhone/iPod"
        },
        {
            string: navigator.platform,
            subString: "Linux",
            identity: "Linux"
        }
    ]

};
BrowserDetect.init();
  if ( BrowserDetect.browser == 'Safari' ) {
        $("#facebook").hide();
  }
if ($(".owl-carousel").length > 0){
  $('.owl-carousel.owl-theme.ph-hide-desctop').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      dots:false,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:5
          }
      }
  })
}
//sertifikaty slider
if ($(".owl-carousel").length > 0){
$('.sert .owl-carousel').owlCarousel({
  margin:30,
  nav:true,
  navSpeed:1000,
  dotsSpeed:1000,
  navText: ["<i class='bi bi-chevron-left'></i>","<i class='bi bi-chevron-right'></i>"],
  slideBy:3,
  responsive:{
      0:{
          items:2,
          center: true,
          loop:false,
          slideBy:1,
          margin:100,
      },
      600:{
          items:2
      },
      1000:{
          items:4
      }
  }
})
}
  //product slider
  /*$('.bundles-mob .owl-carousel').owlCarousel({
    margin:10,
    nav:true,
    navSpeed:1000,
    dotsSpeed:1000,
    slideBy:3,
    afterInit: setWidth(),
    responsive:{
        0:{
            items:2,
            center: true,
            loop:false,
            slideBy:1,
            margin:100,
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
  })
  function setWidth(){
          setTimeout(function(){
              var carWidth = $('.owl-stage').width() + 100;
              $('.owl-stage').width(carWidth);
          },200);

      }*/
      if ($(".owl-carousel").length > 0){
      $('.owl-carousel.products').owlCarousel({
              margin:30,
              nav:true,
              lazyLoad: true,
              navSpeed:1000,
              dotsSpeed:1000,
              navText: ["<i class='bi bi-chevron-left'></i>","<i class='bi bi-chevron-right'></i>"],
              slideBy:3,
              responsive:{
                  0:{
                      items:1.5,
                      center: true,
                      loop:false,
                      slideBy:1,
                      margin:10,
                      nav:false,
                  },
                  450:{
                      stagePadding:30,
                      items:1.55,
                      slideBy:1,
                      nav:false,
                  },
                  600:{
                      stagePadding:50,
                      items:1.9,
                      slideBy:1,
                      nav:false,
                  },
                  750:{
                      items:2.3,
                      stagePadding:20,
                      margin:10,
                      slideBy:1,
                      nav:false,
                      // center: true,
                  },
                  1000:{
                      items:3,
                      stagePadding:0,
                      slideBy:3,
                      margin:10,
                  }
              }
          })
}
if ($(".owl-carousel").length > 0){
    $('.owl-carousel.instagram-slider').owlCarousel({
        nav:true,
        lazyLoad: true,
        navSpeed:1000,
        dotsSpeed:1000,
        navText: ["<i class='bi bi-chevron-left'></i>","<i class='bi bi-chevron-right'></i>"],
        slideBy:3,
        responsive:{
            0:{
                items:1.5,
                center: true,
                loop:false,
                slideBy:1,
                margin:10,
                nav:false,
            },
            450:{
                stagePadding:30,
                items:1.55,
                slideBy:1,
                nav:false,
            },
            600:{
                stagePadding:50,
                items:1.9,
                slideBy:1,
                nav:false,
            },
            750:{
                items:2.3,
                stagePadding:20,
                margin:10,
                slideBy:1,
                nav:false,
                // center: true,
            },
            1000:{
                items:3,
                stagePadding:0,
                slideBy:3,
                margin:10,
            }
        }
    })
}
if ($(".owl-carousel").length > 0){
    $('.owl-carousel.banner-slider').owlCarousel({
        margin:30,
        nav:true,
        lazyLoad: true,
        navSpeed:1000,
        dotsSpeed:1000,
        navText: ["<i class='bi bi-chevron-left'></i>","<i class='bi bi-chevron-right'></i>"],
        items:1,
        center: true,
        loop:false,
        slideBy:1,
        margin:0,
    })
}


$(document).ready(function(){


    $('.button-minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.button-plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });

   var d = [];

          $('.products--main .owl-item.active .product-slider-ecom[data-ecom]').each(function() {

                  d.push({
                    title: $(this).attr("data-ecom-name"),
                    price: $(this).attr("data-ecom-price"),
                    id: $(this).attr("data-ecom-id")
                  });
                  $(this).removeAttr('data-ecom');

          });
          if (d.length > 0) {
              /*console.log(d);*/
             sendDataLayerProductView(d);
          }



$( ".owl-dot, .owl-next" ).click(function(){
	   var d = [];

    setTimeout(function(){
          $('.products--main .owl-item.active .product-slider-ecom[data-ecom]').each(function() {

                  d.push({
                    title: $(this).attr("data-ecom-name"),
                    price: $(this).attr("data-ecom-price"),
                    id: $(this).attr("data-ecom-id")
                  });
                  $(this).removeAttr('data-ecom');

          });
          if (d.length > 0) {
              /*console.log(d);*/
              sendDataLayerProductView(d);
          }
        }, 4000);
	  });
        });
  //category scroll
  var box = $(".category-links"), x;
$(".more-arrow").click(function() {
    x = ((box.width() / 1)) + box.scrollLeft();
    box.animate({
      scrollLeft: x,
    })
})


// $(function() {
//     $("img.lazy").Lazy({
//         threshold: 0,
//         effect: 'fadeIn',
//         effectTime: 100,
//         afterLoad: function(element) {
//             element.css("filter", "blur(0) grayscale(0)");
//         }
//     });
// });
if ($("#modal-sinevo").length > 0){
  window.addEventListener('load', function() {
     setTimeout(function() {
         $('#modal-sinevo').modal('show');
     }, 10000);
  });
}
// Cache selectors
var lastId,
    topMenu = $(".plan-single ul"),
    topMenuHeight = topMenu.outerHeight()+15,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    });

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
  var href = $(this).attr("href"),
      offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
  $('html, body').stop().animate({
      scrollTop: offsetTop
  }, 300);
  e.preventDefault();
});

// Bind to scroll
$(window).scroll(function(){
   // Get container scroll position
   var fromTop = $(this).scrollTop()+topMenuHeight;

   // Get id of current scroll item
   var cur = scrollItems.map(function(){
     if ($(this).offset().top < fromTop)
       return this;
   });
   // Get the id of the current element
   cur = cur[cur.length-1];
   var id = cur && cur.length ? cur[0].id : "";

   if (lastId !== id) {
       lastId = id;
       // Set/remove active class
       menuItems
         .parent().removeClass("active")
         .end().filter("[href='#"+id+"']").parent().addClass("active");
   }
});
